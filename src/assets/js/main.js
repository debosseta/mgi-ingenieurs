import AOS from 'aos';
//import PhotoSwipe from "photoswipe";
//import PhotoSwipeUI_Default from 'photoswipe/dist/photoswipe-ui-default';
import mixitup from 'mixitup';
import lazysizes from 'lazysizes';
import unveilhooks from 'lazysizes/plugins/unveilhooks/ls.unveilhooks';
//import Masonry from '../../../node_modules/masonry-layout';
// Animations
AOS.init({
  // Global settings
  disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
  startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
  initClassName: 'aos-init', // class applied after initialization
  animatedClassName: 'aos-animate', // class applied on animation
  useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
  disableMutationObserver: false, // disables automatic mutations' detections (advanced)
  debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
  throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)
  // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
  offset: 120, // offset (in px) from the original trigger point
  delay: 0, // values from 0 to 3000, with step 50ms
  duration: 700, // values from 0 to 3000, with step 50ms
  easing: 'ease', // default easing for AOS animations
  once: true, // whether animation should happen only once - while scrolling down
  mirror: false, // whether elements should animate out while scrolling past them
  anchorPlacement: 'top-bottom', // defines which position of the element regarding to window should trigger the animation
});
// Gallerie
jQuery(function ($) {

  $(".gallery .row-photos").lightGallery({
    thumbnail:false,
    selector: '.block-photo a'
  }); 


 /* $('.filter').on('click', 'button', function () {
    $('.filter .filter-btn.active').removeClass('active');
    $(this).addClass('active');
  });
  if ($('.filters-result').length) {
    mixitup('.filters-result', {
      animation: {
        effects: 'fade',
        duration: 400
      },
      classNames: {
        block: 'filter',
        elementFilter: 'filter-btn'
      },
      selectors: {
        target: '.block-photo'
      }
    });
  }*/
  $('.menu').click(function () {
    $('.menu-container').toggleClass('active');
  })
  var header = $("header");
  $(window).scroll(function () {
    var scroll = $(window).scrollTop();
    if (scroll >= 150) {
      header.addClass("active");
    } else {
      header.removeClass("active");
    }
  });
  
});
var map;
var ajaxRequest;
var plotlist;
var plotlayers = [];
// Init Open Street Maps
function initmap() {
  // set up the map
  map = new L.Map('map');
  var osmUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
  var osmAttrib = 'Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors';
  var osm = new L.TileLayer(osmUrl, {
    minZoom: 2,
    maxZoom: 19,
    attribution: osmAttrib
  });
  map.setView(new L.LatLng(46.69470, 6.91494), 10);
  map.addLayer(osm);
  var marker = L.marker([46.52640, 6.90064]).addTo(map);
  marker.bindPopup("<b>MGI ingénieurs</b><br/>Rte de Vevey 128<br/>1618 Châtel-St-Denis").openPopup();
  var marker2 = L.marker([46.61595, 7.05747]).addTo(map);
  marker2.bindPopup("<b>MGI ingénieurs</b><br/>Rue de Gruyères 53<br/>1630 Bulle").openPopup();
  var marker3 = L.marker([46.80052, 7.12446]).addTo(map);
  marker3.bindPopup("<b>MGI ingénieurs</b><br/>Rte du Petit-Moncor 14<br>1752 Villars-sur-Glâne").openPopup();
  var marker4 = L.marker([46.69470, 6.91494]).addTo(map);
  marker4.bindPopup("<b>MGI ingénieurs</b><br/>Av. Gérard Clerc 6<br/>1680 Romont").openPopup();
}
if (document.querySelector('#map')) {
  initmap();
}
$(function () {
  if ($(".numbers").length) {
    var viewed = false;
    testScroll();
    $(window).scroll(testScroll);
    function isScrolledIntoView(elem) {
      var docViewTop = $(window).scrollTop();
      var docViewBottom = docViewTop + $(window).height();
      var elemTop = $(elem).offset().top;
      var elemBottom = elemTop + $(elem).height();
      return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }
    function testScroll() {
      if (isScrolledIntoView($(".numbers")) && !viewed) {
        viewed = true;
        $('.value').each(function () {
          $(this).prop('Counter', 0).animate({
            Counter: $(this).data("value")
          }, {
            duration: 1400,
            easing: 'swing',
            step: function (now) {
              $(this).text(Math.ceil(now));
            }
          });
        });
      }
    }
  }
});
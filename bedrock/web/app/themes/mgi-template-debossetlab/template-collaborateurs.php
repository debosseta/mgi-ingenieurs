<?php /* Template Name: Collaborateurs */ ?>
<?php global $post; ?>
<?php get_header(); ?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<?php $home_title = get_the_title( get_option('page_on_front') ); ?>
<?php
    $image = get_field('bandeau_image_de_fond');
    $thumb = '';
    if( $image ):
        // Thumbnail size attributes.
        $size = 'home-1920-500';
        $thumb = $image['sizes'][ $size ];
    endif;
    ?>
<section id="projet-title" class="small lazyload" data-bg="<?php echo $thumb; ?>">
  <div class="container-image">
    <div class="fakeimg"></div>
  </div>
  <div class="background"></div>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
   
          <?php  $parentID = $post->post_parent; ?>
      
		<ul class="breadcrum" itemscope itemtype="http://schema.org/BreadcrumbList">
              <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<a itemprop="item" href="<?php echo get_home_url(); ?>">
			<span itemprop="name"><?php echo $home_title; ?></span>	
		</a> 
		<meta itemprop="position" content="1" />
	      </li>
 <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<a itemprop="item" href="<?php echo esc_url( get_page_link( $parentID ) ); ?>">
			<span itemprop="name"><?php echo get_the_title( $parentID ); ?></span>	
		</a> 
		<meta itemprop="position" content="2" />
	      </li>
              <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<span itemprop="name"><?php the_title(); ?></span>
		<meta itemprop="position" content="3" />
	      </li>
            </ul>
        <div class="title" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
          <h1><?php the_title(); ?></h1>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="collaborateur-content">
  <div id="introduction">
    <div class="container link">
      <div class="row">
        <div class="col-lg-8 offset-lg-2">
          <div class="content" data-aos="fade-up" data-aos-delay="0">
            <div class="info center">
              <h3><?php the_field('introduction', false, false); ?></h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="teams" class="teamsbottom">
    <div class="container link">
      <div class="row">




        <?php $custom_terms = get_terms('position');

    foreach($custom_terms as $custom_term) :
        wp_reset_query();
        $args = array('post_type' => 'collaborateurs',
            'tax_query' => array(
                array(
                    'taxonomy' => 'position',
                    'field' => 'slug',
                    'terms' => $custom_term->slug,
                ),
            ),
        );

        $loop = new WP_Query($args);
        if($loop->have_posts()) : ?>

        <div class="col-lg-12 center block-teams">
          <h2 data-aos="fade-up" data-aos-delay="200"><?php echo $custom_term->name; ?></h2>
          <div class="row padding collaborateurs" data-aos="fade-up" data-aos-delay="400">

            <?php while($loop->have_posts()) : $loop->the_post(); ?>

            <?php
        $image = get_field('photo_collaborateur');
        $thumb = '';
        if( $image ):
            // Thumbnail size attributes.
            $size = '283-283';
            $thumb = $image['sizes'][ $size ];

        else:
            $thumb = get_template_directory_uri().'/assets/img/unknow.png';
        endif;
        ?>
            <div class="collaborateur col-lg-3 col-sm-6">
              <div class="collaborateur-block">
                <img src="<?php echo $thumb; ?>" alt="<?php the_title(); ?>" />
                <div class="collaborateur-block-content">
                  <h3><?php the_title(); ?></h3>
                  <div class="fonction"><?php the_field('fonction'); ?></div>
                  <div class="formation"><?php the_field('formation'); ?></div>
                  <ul>
<?php if( get_field('telephone_1')){ ?>
                    <li class="telephone"><?php the_field('telephone_1'); ?><?php if( get_field('telephone_2')){ ?><br />
                      <?php the_field('telephone_2'); ?><?php } ?>
                    </li><?php } ?>
                    <?php if( get_field('email')){ ?><li class="mail"><a href="mailto:<?php echo antispambot( get_field('email') ) ; ?>"><?php echo antispambot( get_field('email') ) ; ?></a></li><?php } ?>
                  </ul>
                </div>
              </div>
            </div>
            <?php endwhile; ?>
          </div>
        </div>
        <?php
    endif;
endforeach; 
wp_reset_postdata();?>

      </div>
    </div>
  </div>
</section>
<?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>
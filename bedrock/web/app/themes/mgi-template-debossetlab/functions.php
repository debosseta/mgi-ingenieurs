<?php 
/* 
Main Function wordpress
This is where we call all our function to keep a good order in our dev
Just add what you need to
*/
require __DIR__.'/functions/assets_managment.php'; // Themes call for CSS and Javascript
require __DIR__.'/functions/remove_comment.php'; // Delete defaut wordpress comments
require __DIR__.'/functions/custom_login.php'; // Custom login with mgi logo + color
require __DIR__.'/functions/remove_dashboard.php'; // Remove staff we don't need
require __DIR__.'/functions/poste_types.php'; // Add customs poste type
require __DIR__.'/functions/menu.php'; // Add wp menu
require __DIR__.'/functions/tinymce.php'; // tinymce custom
require __DIR__.'/functions/thumbnail_sizes.php';
require __DIR__.'/functions/telechargement.php';
require __DIR__.'/functions/pagination.php';
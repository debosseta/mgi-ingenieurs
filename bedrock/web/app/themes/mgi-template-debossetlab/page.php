<?php get_header(); ?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<?php $home_title = get_the_title( get_option('page_on_front') ); ?>
<?php
    $image = get_field('bandeau_image_de_fond');
    $thumb = '';
    if( $image ):
        // Thumbnail size attributes.
        $size = 'home-1920-500';
        $thumb = $image['sizes'][ $size ];
    endif;
    $pageIDJob = get_the_ID();
    ?>

<section id="projet-title" class="small lazyload" data-bg="<?php echo $thumb; ?>">
      <div class="container-image">
        <div class="fakeimg"></div>
      </div>
      <div class="background"></div>
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <ul class="breadcrum">
                <li><a href="<?php echo get_home_url(); ?>"><?php echo $home_title; ?></a></li>
              <li><?php the_title(); ?></li>
            </ul>
            <div class="title" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
              <h1><?php the_title(); ?></h1>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    
    <section id="jobs-content" data-aos="fade-up" data-aos-delay="300">
      <div class="container link">
        <div class="row">
          <div class="col-lg-12">
                     <div class="content" data-aos="fade-up" data-aos-delay="400">
                        <?php the_field('contenu'); ?>
                      </div>
          </div>
            <!-- -->
          </div>
        </div>
      </div>
    </section>
    <?php endwhile; ?>
            <?php endif; ?>
            <?php get_footer(); ?>
<?php get_header(); ?>

<?php $home_title = get_the_title( get_option('page_on_front') ); ?>






<?php


$term = get_queried_object();


// vars
$imageterm = get_field('bandeau_image_de_fond', $term);



              $args = array(
                'post_type' => 'page',
                'post__in' => array(43)
              );
              $query = new WP_Query( $args );
              if ( $query->have_posts() ) {
                  while ( $query->have_posts() ) {
                      $query->the_post();

if($imageterm){
$thumb = '';
    if( ($imageterm)):
        // Thumbnail size attributes.
        $size = 'home-1920-500';
        $thumb = $imageterm['sizes'][ $size ];
    endif;
} else {

    $image = get_field('bandeau_image_de_fond');
    $thumb = '';
    if( $image ):
        // Thumbnail size attributes.
        $size = 'home-1920-500';
        $thumb = $image['sizes'][ $size ];
    endif;

}
    ?>

<section id="projet-title" class="small lazyload" data-bg="<?php echo $thumb; ?>">



<?php } } wp_reset_postdata();?>


      <div class="container-image">
        <div class="fakeimg"></div>
      </div>
      <div class="background"></div>
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
		<ul class="breadcrum" itemscope itemtype="http://schema.org/BreadcrumbList">
              <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<a itemprop="item" href="<?php echo get_home_url(); ?>">
			<span itemprop="name"><?php echo $home_title; ?></span>	
		</a> 
		<meta itemprop="position" content="1" />
	      </li>
		<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<a itemprop="item" href="<?php echo esc_url( get_page_link( 43 ) ); ?>">
			<span itemprop="name"><?php echo get_the_title( 43 ); ?></span>	
		</a> 
		<meta itemprop="position" content="2" />
	      </li>
              <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<span itemprop="name"><?php single_term_title(); ?></span>
		<meta itemprop="position" content="3" />
	      </li>
            </ul>
            <div class="title" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
              <h1><?php echo get_the_title( 43 ); ?> - <?php single_term_title(); ?></h1>
            </div>
          </div>
        </div>
      </div>
    </section>


    <section id="projet-liste">

    <div class="container">
            <div class="row">
            <div class="col-sm-12" data-aos="fade-up">
            <div class="filter">
            <a class="filter-btn" href="<?php echo esc_url( get_page_link( 43 ) ); ?>"><?php echo get_the_title( 43 ); ?></a>

            <?php $currentterm = get_term_by( 'slug', get_query_var( 'type' ), get_query_var( 'taxonomy' ) );  ?>
              
       
<?php 

$terms = get_terms('type');

foreach ($terms as $term) {
  $class = $currentterm->slug == $term->slug ? 'active' : '' ; 

echo '<a class="filter-btn '.$class .'" href="' . esc_url( get_page_link( 43 ) ) . $term->slug . '">'.$term->name.'</a>';
}


$term_slug = get_queried_object()->slug;
    if ( !$term_slug )
    return;
    else  
     
?>
</div>
            </div>
            </div>


        <div class="row filters-result" data-aos="fade-up">

        <?php

              $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
              $args = array(
                'post_type' => 'projets',
                'post_status' => 'publish',
                'tax_query' => array(
                  array(
                    'taxonomy' => 'type',
                    'field' => 'slug',
                    'terms' => $term_slug
                  )
                  ),
                  'posts_per_page' => 50, 
                  'paged' => $paged
              );
              $query = new WP_Query( $args );
              if ( $query->have_posts() ) :
                  while ( $query->have_posts() ) :
                      $query->the_post();
                     
                      $thumb2 = get_field('photo_a_la_une');
                      if( $thumb2 ):
                          // Thumbnail size attributes.
                          $size = '768-500';
                          $thumb2 = $thumb2['sizes'][ $size ];
                      endif; if( have_rows('informations') ): ?>
                        <?php while( have_rows('informations') ): the_row(); ?>

                       

          <div class="col-md-6 col-lg-4 block-photo <?php foreach(get_the_terms($query->post->ID, 'type') as $term)
         echo $term->slug . ' '; ?>">
            <a href="<?php the_permalink(); ?>">
              <div>
                <div class="thumbnail-container">
                  <div class="thumbnail lazyload" data-bg="<?php echo $thumb2; ?>">
                    <div class="container-image">
                      <div class="fakeimg"></div>
                    </div>
                  </div>
                </div>
                <div class="content">
                  <div class="content-info">
                    <div class="date">
                      <i class="fa fa-calendar" aria-hidden="true"></i><?php the_sub_field('date'); ?>
                    </div>
                    <div class="lieu">
                      <i class="fa fa-map-marker" aria-hidden="true"></i><?php the_sub_field('localisation'); ?>
                    </div>
                  </div>
                  <h2><?php echo str_replace(' | ', '<br />', get_the_title()); ?></h2>
                </div>
              </div>
            </a>
          </div><!-- projets-->
          <?php endwhile; ?>
            <?php endif; ?>

          <?php endwhile; ?>
            <?php endif; ?>

            <?php if (function_exists("pagination")) {?>
            <div class="col-sm-12">
              <?php pagination($query->max_num_pages); ?>
            </div>
            <?php } ?>          

        </div>
</section>
       
            <?php get_footer(); ?>
    </main>
    <footer>
      <div class="container footer-top">
        <div class="row">
          <div class="col-lg-12">
            <a class="logo" href="<?php echo get_home_url();?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-white.svg" alt="<?php wp_title(''); ?>" /></a>
            <div class="row">
              <div class="col-12">
                <div class="line"></div>
              </div>
              <?php
              $args = array(
                'page_id' => '20' // Page contact
              );
              $query = new WP_Query( $args );
              if ( $query->have_posts() ) {
                  while ( $query->have_posts() ) {
                      $query->the_post();
                      if( have_rows('bureau_listes') ):
                        while( have_rows('bureau_listes') ) : the_row(); ?>
                          <div class="col-sm-6 col-md-4 col-lg-3">
                            <ul>
                                <li class="adresse"><?php the_sub_field('rue'); ?><br /><?php the_sub_field('code_postal_ville'); ?></li>
                                <li class="telephone"><?php the_sub_field('telephone'); ?></li>
                            <ul>
                          </div>
                        <?php endwhile;
                      endif;
                  }
              }
              // Restore original post data.
              wp_reset_postdata();
              ?>
            </div>
          </div>
        </div>
      </div>
      <div class="footer-bottom link">
        <div class="container">
          <div class="row">
          </div>
        </div>
      </div>
    </footer>
    <?php wp_footer(); ?>
  </body>
</html>
jQuery(function($){
  // Hide generate download link
  $("#submit_telechargements").hide();
  // set clipboard on .copy class
  var clipboard = new ClipboardJS('.copy');
  // Disabled default automatic dropzone
  Dropzone.autoDiscover = false;
  // If dropzone existe
  if($('.dropzone').length !== 0){
    // New dropzone
    var uploadFiles = new Dropzone(".dropzone", { 
      // All settings 1 du to server requirement
      autoProcessQueue: true,
      url: ajaxurl,
      parallelUploads: 1,
      maxFilesize: 2000,
      timeout: 300000,
      uploadMultiple: false,
      addRemoveLinks: false, 
      dictDefaultMessage: "Glissez vos fichiers ici",
      dictFallbackMessage: "Votre naviguateur ne supporte pas le drag and drop",
      dictFallbackText: "Utiliser l'ancien navigateur",
      dictFileTooBig: "Le fichier est top volumineux ({{filesize}} mb). Taille max de fichier: {{maxFilesize}}mb.",
      dictInvalidFileType: "Vous ne pouvez pas uploader de fichier de ce type",
      dictResponseError: "Le serveur {{statusCode}} code.",
      dictCancelUpload: "Annuler l'upload",
      dictCancelUploadConfirmation: "Etes vous sûr de vouloir annuler l'upload",
      dictRemoveFile: "Supprimer le fichier",
      dictMaxFilesExceeded: "Vous ne pouvez pas ajouter plus de fichiers. Maximum 100",
      // When complete display submit telechargement
      init: function () {
        this.on("complete", function (file) {
          if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
            $("#submit_telechargements").show();
          }
        });
      },
      // On success append folder random name mgi-xxxxxxxxxxxxxxxxxxxxxxxxxxx
      success: function (file, response) {
        var formData = new FormData();
        formData.append('folderrandom' , $('#folderrandom').val());
      },
      // TO be able to save the path of a folder uploaded we add the value to the #path input hidden
      sending: function(file){
        if(typeof file.fullPath !== 'undefined'){
          console.log(file.fullPath);
          var changePath = file.fullPath.split("/");
          var firstFolder = changePath[0];
          //changePath[0] = $('#folderrandom').val();
          changePath[0] = firstFolder;
          changePath = changePath.join("/");
          changePath = changePath.substr(0,changePath.lastIndexOf("/")+1);
          $('#path').val(changePath);
          console.log('Folder path : ' + changePath);
        } else {
          $('#path').val('');
          console.log('Folder path : nofolder');
        }
      }
    });
  }

  $("#submit_telechargements").click(function(){
    $(this).attr("disabled", true);
    var ajaxurl = ajax_var.url;
    var data = {
      'action': 'generateZip',
      'tmpName' : $('#folderrandom').val()
    };

    $.ajax({
      type: 'POST',
      url: ajaxurl,
      data: data,
      success: function(data) {

        $('#submit_telechargements').hide();
        $('#telechargement').append(data);
        $('#uploadingtelechargements').hide();

      },
      error: function() {
        alert('Une erreur est survenue veuillez ressayer puis avertir debosset@gmail.com'); }
    });    






  });

/*
  $("#telechargerZip").click(function(){
    var ajaxurl = ajax_var.url;
    var data = {
      'action': 'downloadZip',
      'nonce' : ajax_var.nonce,
      'zip': $(this).data('zipfichier')
    };
  });

*/
});
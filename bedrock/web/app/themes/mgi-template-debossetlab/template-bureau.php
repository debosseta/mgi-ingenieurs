<?php /* Template Name: Bureau */ ?>
<?php get_header(); ?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<?php $home_title = get_the_title( get_option('page_on_front') ); ?>
<?php
    $image = get_field('bandeau_image_de_fond');
    $thumb = '';
    if( $image ):
        // Thumbnail size attributes.
        $size = 'home-1920-500';
        $thumb = $image['sizes'][ $size ];
    endif;
    ?>
<section id="projet-title" class="small lazyload" data-bg="<?php echo $thumb; ?>">
      <div class="container-image">
        <div class="fakeimg"></div>
      </div>
      <div class="background"></div>
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <ul class="breadcrum" itemscope itemtype="http://schema.org/BreadcrumbList">
              <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<a itemprop="item" href="<?php echo get_home_url(); ?>">
			<span itemprop="name"><?php echo $home_title; ?></span>	
		</a> 
		<meta itemprop="position" content="1" />
	      </li>
              <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<span itemprop="name"><?php the_title(); ?></span>
		<meta itemprop="position" content="2" />
	      </li>
            </ul>
            <div class="title" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
              <h1><?php the_title(); ?></h1>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="bureau-content">
      <div id="introduction" class="background-grey">
        <div class="container link">
          <div class="row">
            <div class="col-lg-8 offset-lg-2">
              <div class="content" data-aos="fade-up" data-aos-delay="0">
                <div class="info center">
                  <h3><?php the_field('introduction'); ?></h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="chiffre" class="container padding">
        <div class="row">
          <div class="col-lg-12 center">
            <h2 data-aos="fade-up" data-aos-delay="200"><?php the_field('chiffres_clefs'); ?></h2>
            <div class="row padding numbers" data-aos="fade-up" data-aos-delay="400">
            <?php
            // Check rows exists.
            if( have_rows('chiffre_clefs_block') ):
                // Loop through rows.
                while( have_rows('chiffre_clefs_block') ) : the_row();
                ?>
                    <div class="col-lg-3">
                        <div class="chiffre-clef"><i class="icon-<?php the_sub_field('chiffre_icon'); ?>"></i>
                        <div class="chiffre-clef-number"><span class="value" data-value="<?php the_sub_field('chiffre_nombre'); ?>">0</span></div>
                        <div class="chiffre-clef-label"><?php the_sub_field('chiffre_label'); ?></div>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
      
      <div id="teams">
        <div class="container link padding">
          <div class="row">

          <?php 
                    $args = array('post_type' => 'collaborateurs',
                    'tax_query' => array(
                            array(
                                'taxonomy' => 'position',
                                'field' => 'slug',
                                'terms' => 'direction',
                            ),
                        ),
                    );

                    $loop = new WP_Query($args);
                    if($loop->have_posts()) : ?>
                        <div class="col-lg-12 center">
                        <h2 data-aos="fade-up" data-aos-delay="200">la Direction</h2>
                        <div class="row padding collaborateurs" data-aos="fade-up" data-aos-delay="400">
                        <?php while($loop->have_posts()) : $loop->the_post(); ?>

                        <?php
                        $image = get_field('photo_collaborateur');
                        $thumb = '';
                        if( $image ):
                            // Thumbnail size attributes.
                            $size = '283-283';
                            $thumb = $image['sizes'][ $size ];
                        endif;
                        ?>
                        <div class="collaborateur col-lg-3 col-sm-6">
                            <div class="collaborateur-block">
                                <img src="<?php echo $thumb; ?>" alt="<?php the_title(); ?>" />
                                <div class="collaborateur-block-content">
                                <h3><?php the_title(); ?></h3>
                                <div class="fonction"><?php the_field('fonction'); ?></div>
                                <div class="formation"><?php the_field('formation'); ?></div>
                                <ul>
                                    <li class="telephone"><?php the_field('telephone_1'); ?><br />
                                    <?php the_field('telephone_2'); ?>
                                    </li>
                                    <li class="mail"><a href="mailto:<?php echo antispambot( get_field('email') ) ; ?>"><?php echo antispambot( get_field('email') ) ; ?></a></li>
                                </ul>
                                </div>
                            </div>
                        </div>
                        <?php endwhile;
                    endif;
                wp_reset_postdata();?>
                </div>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-lg-12 center margin-cta">
              <a class="cta" href="<?php the_field('lien_page'); ?>"><?php the_field('lien_texte'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
            </div>
          </div>
        </div>
      </div>
            </div>
            </div>
       

      <div id="managment" class="container padding">
        <div class="row">
          <div class="col-lg-12 center">
            <h2 data-aos="fade-up" data-aos-delay="0"><?php the_field('iso_titre'); ?></h2>
          </div>
          <div class="col-lg-8 offset-lg-2 margin-top">
            <div data-aos="fade-up" data-aos-delay="0">
            <?php the_field('iso_description'); ?>
            </div>
            <div class="certification" data-aos="fade-up" data-aos-delay="200">
              <div class="certification-wrap">

              <?php
            // Check rows exists.
            if( have_rows('iso') ):
                // Loop through rows.
                while( have_rows('iso') ) : the_row();
                ?>
                <?php
                    $image = get_sub_field('image');
                    $thumb = '';
                    if( $image ):
                        // Thumbnail size attributes.
                        $size = '200-200';
                        $thumb = $image['sizes'][ $size ];
                    endif;
                    ?>
                    <div class="iso">
                        <a href="<?php the_sub_field('lien'); ?>" target="_blank">
                            <img src="<?php echo $thumb; ?>" alt="<?php the_sub_field('label'); ?>" />
                            <?php the_sub_field('label'); ?>
                        </a>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
                
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="association">
        <div class="container padding">
          <div class="row">
            <div class="col-lg-12 center">
              <h2 data-aos="fade-up" data-aos-delay="0"><?php the_field('associations_titre'); ?></h2>
              <div class="association-wrap" data-aos="fade-up" data-aos-delay="200">
                <ul>
                <?php
                // Check rows exists.
                if( have_rows('liste_association') ):
                    // Loop through rows.
                    while( have_rows('liste_association') ) : the_row();

                        $image = get_sub_field('image');
                        $thumb = '';
                        $alt = '';
                        if( $image ):

                            $alt = $image['alt'];
                            // Thumbnail size attributes.
                            $size = '200-200';
                            $thumb = $image['sizes'][ $size ];
                        endif; ?>

                        <li><a href="<?php the_sub_field('url'); ?>" target="_blank"><img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" class="lazyload" /></a></li>
                    <?php endwhile; ?>
                <?php endif; ?> 
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="politique" class="container padding">
        <div class="row">
          <div class="col-lg-12 center margin-bottom">
            <h2 data-aos="fade-up" data-aos-delay="0"><?php the_field('politique_de_bureau_titre'); ?></h2>
          </div>
          <div class="col-md-6">
            <div data-aos="fade-up" data-aos-delay="200">
                <?php the_field('contenu_gauche'); ?>    
            </div>
          </div>
          <div class="col-md-6">
            <div data-aos="fade-up" data-aos-delay="200">
                <?php the_field('contenu_droite'); ?>
            </div>
          </div>
        </div>
      </div>
    </section>
<?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>
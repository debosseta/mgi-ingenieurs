<?php get_header(); ?>
<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
    <?php
    $image = get_field('bloc_bureau_photo');
    if( $image ):
        // Thumbnail size attributes.
        $size = 'home-1920-500';
        $thumb = $image['sizes'][ $size ];
    ?>
    <section class="head-bureau lazyload" data-bg="<?php echo $thumb; ?>">
        <div class="container-image">
            <div class="fakeimg"></div>
        </div>
        <div class="background"></div>
        <div class="container">
            <div class="row">
            <div class="col-lg-8 offset-lg-2 center" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                <h1><?php the_field('bloc_bureau_texte'); ?></h1>
                <a href="<?php the_field('bloc_bureau_lien'); ?>" class="cta white"><?php the_field('bloc_bureau_label'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
            </div>
            </div>
        </div>
        </section>
    <?php endif; ?>
    <section id="services-content" class="head-services" data-aos="fade-up" data-aos-delay="300">
      <div class="services-content">
        <div class="container link">
          <div class="row">
            <div class="col-lg-12 titlehome">
              <h2 data-aos="fade-up" data-aos-delay="200"><?php the_field('bloc_services_titre'); ?></h2>
            </div>
            <div class="col-lg-8 offset-lg-2">
              <div class="" data-aos="fade-up" data-aos-delay="400">
                <div class="info center">
                  <h3><?php the_field('bloc_service_texte'); ?></h3>
                </div>
              </div>
            </div>

            <?php
            // Check rows exists.
            if( have_rows('services') ):
                // Loop through rows.
                while( have_rows('services') ) : the_row();
                ?>
                <?php
                $image = get_sub_field('photo');
                if( $image ):
                    // Thumbnail size attributes.
                    $size = '768-500';
                    $thumb = $image['sizes'][ $size ];
                ?>
                <div class="col-md-6 col-lg-4 services-content-single">
                    <div class="thumbnail lazyload" data-bg="<?php echo $thumb; ?>">
                        <div class="container-image">
                            <div class="fakeimg"></div>
                        </div>
                        <div class="infobox">
                            <ul>
                            <?php
                            if( have_rows('liste') ):
                                while( have_rows('liste') ) : the_row();
                                ?>
                                <li><?php the_sub_field('texte'); ?></li>    
                                <?php
                                endwhile;
                            endif;
                            ?>
                            </ul>
                        </div>
                    </div>
                    <div class="content">
                        <h3><?php the_sub_field('titre'); ?></h3>
                        <ul class="mobileinfobox">
                            <?php
                            if( have_rows('liste') ):
                                while( have_rows('liste') ) : the_row();
                                ?>
                                <li><?php the_sub_field('texte'); ?></li>    
                                <?php
                                endwhile;
                            endif;
                            ?>
                        </ul>
                    </div>
                </div><!-- service -->
                <?php endif; ?>       
                <?php
                // End loop.
                endwhile;
            endif;
            ?>
      </div>
    </section>
    <section id="projet-liste" class="head-projets">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 titlehome" data-aos="fade-up">
            <h2 data-aos="fade-up" data-aos-delay="200"><?php the_field('titre_projet_accueil'); ?></h2>
          </div>
        </div>
        <div class="row filters-result" data-aos="fade-up">

        <?php
              $args = array(
                'post_type' => 'projets',
                'post_status' => 'publish',
                'posts_per_page' => 3, 
              );
              $query = new WP_Query( $args );
              if ( $query->have_posts() ) {
                  while ( $query->have_posts() ) {
                      $query->the_post();
                     
                      $thumb2 = get_field('photo_a_la_une');
                      if( $thumb2 ):
                          // Thumbnail size attributes.
                          $size = '768-500';
                          $thumb2 = $thumb2['sizes'][ $size ];
                      endif;
                      
                       if( have_rows('informations') ): ?>
                        <?php while( have_rows('informations') ): the_row(); ?>
                     

                        


                          <div class="col-md-6 col-lg-4 block-photo geniecivil">
                            <a href="<?php the_permalink(); ?>">
                            <div>
                                <div class="thumbnail-container">
                                <div class="thumbnail lazyload" data-bg="<?php echo $thumb2; ?>">
                                    <div class="container-image">
                                    <div class="fakeimg"></div>
                                    </div>
                                </div>
                                </div>
                                <div class="content">
                                <div class="content-info">
                                    <div class="date">
                                    <i class="fa fa-calendar" aria-hidden="true"></i><?php the_sub_field('date'); ?>
                                    </div>
                                    <div class="lieu">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i><?php the_sub_field('localisation'); ?>
                                    </div>
                                </div>
                                <h2><?php echo str_replace(' | ', '<br />', get_the_title()); ?></h2>
                                </div>
                            </div>
                            </a>
                        </div><!-- block-photo -->
                        <?php 
                   
                 
                         endwhile;
                      endif;
                  }
              }
              // Restore original post data.
              wp_reset_postdata();
              ?>


         
          <!-- end -->
          <div class="col-lg-12 center"> <a href="<?php the_field('bloc_projet_lien'); ?>" class="cta"><?php the_field('bloc_projet_label'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a></div>
        </div>
      </div>
    </section>
    <section id="news" class="head-news">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 titlehome" data-aos="fade-up">
            <h2 data-aos="fade-up" data-aos-delay="200"><?php the_field('titre_actualite_accueil'); ?></h2>
          </div>

          <?php
              $args = array(
                'post_type' => 'actualites',
                'post_status' => 'publish',
                'posts_per_page' => 3, 
              );
              $query = new WP_Query( $args );
              if ( $query->have_posts() ) :
                  while ( $query->have_posts() ) :
                      $query->the_post();?>

                     
                      

          <div class="col-lg-8 offset-lg-2 news" data-aos="fade-up">
          <?php $thumb2 = get_field('actu_photo_a_la_une');
                      if( $thumb2 ):
                          // Thumbnail size attributes.
                          $size = 'large';
                          $thumb2 = $thumb2['sizes'][ $size ]; ?>
                            <div class="thumbnail-container">
                                <img class="thumbnail lazyload" src="<?php echo $thumb2; ?>">
                                <div class="container-image">
                                    <div class="fakeimg"></div>
                                </div>
                            </div>
                          <?php 
                      endif; ?>
            <div class="content">
              <div class="content-info">
                <div class="date">
                  <i class="fa fa-calendar" aria-hidden="true"></i><?php echo get_the_date( 'd.m.Y' ); ?>
                </div>
              </div>
              <h2><?php the_title(); ?></h2>
              <?php the_field('actu_contenu'); ?>
              <?php if( have_rows('galerie_image') ):?>
        <div class="gallery">
                  <div class="row row-photos">
                  <?php while( have_rows('galerie_image') ) : the_row(); ?>


<?php
    $image = get_sub_field('photo');
    $thumb = '';
    if( $image ):
        $size = '518-245';
        $thumb = $image['sizes'][ $size ];
        $size = 'large';
        $large = $image['sizes'][ $size ];
        $height = $image['sizes'][ 'large-height' ];
        $width = $image['sizes'][ 'large-width' ];
    endif;

    ?>
                 <div class="col-sm-6 col-lg-4 block-photo" data-aos="fade" data-aos-delay="0">
            <a href="<?php echo $large; ?>" data-download-url="false" data-width="<?php echo $width; ?>" data-height="<?php echo $height; ?>">
              <div class="thumbnail lazyload" data-bg="<?php echo $thumb; ?>">
                <div class="container-image">
                  <div class="fakeimg"></div>
                </div>
              </div>
            </a>
          </div>
                    
                 
                <?php endwhile; ?>
   </div>
   </div>
                <?php endif; ?>
            </div>
          </div><!-- / news -->
          <?php
          endwhile;
          endif; 
          wp_reset_postdata();?>
          <div class="col-lg-12 center"> <a href="<?php the_field('bloc_actualite_lien'); ?>" class="cta"><?php the_field('bloc_actualite_label'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a></div>
        </div>
    </section>
    <?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>
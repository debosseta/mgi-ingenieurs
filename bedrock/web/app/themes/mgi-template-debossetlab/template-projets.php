<?php /* Template Name: Projets */ ?>
<?php get_header(); ?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<?php $home_title = get_the_title( get_option('page_on_front') ); ?>
<?php
    $image = get_field('bandeau_image_de_fond');
    $thumb = '';
    if( $image ):
        // Thumbnail size attributes.
        $size = 'home-1920-500';
        $thumb = $image['sizes'][ $size ];
    endif;
    ?>

<section id="projet-title" class="small lazyload" data-bg="<?php echo $thumb; ?>">
      <div class="container-image">
        <div class="fakeimg"></div>
      </div>
      <div class="background"></div>
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
           <ul class="breadcrum" itemscope itemtype="http://schema.org/BreadcrumbList">
              <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<a itemprop="item" href="<?php echo get_home_url(); ?>">
			<span itemprop="name"><?php echo $home_title; ?></span>	
		</a> 
		<meta itemprop="position" content="1" />
	      </li>
              <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<span itemprop="name"><?php the_title(); ?></span>
		<meta itemprop="position" content="2" />
	      </li>
            </ul>
            <div class="title" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
              <h1><?php the_title(); ?></h1>
            </div>
          </div>
        </div>
      </div>
    </section>


    <section id="projet-liste">

    <div class="container">
            <div class="row">
            <div class="col-sm-12" data-aos="fade-up">
                <div class="filter">

               

              
                <?php $terms = get_terms('type');

foreach ($terms as $term) {



  
    echo '<a class="filter-btn" href="' . esc_url( get_page_link( 43 ) ) . $term->slug . '">'.$term->name.'</a>';
}
 ?>
    

                     
           
                </div>
            </div>
            </div>


        <div class="row filters-result" data-aos="fade-up">

        <?php
        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
        ?>


        <?php
              $args = array(
                'post_type' => 'projets',
                'post_status' => 'publish',
                'posts_per_page' => 21, 
                'paged' => $paged
              );
              $query = new WP_Query( $args );
              if ( $query->have_posts() ) :
                  while ( $query->have_posts() ) :
                      $query->the_post();
                     
                      $thumb2 = get_field('photo_a_la_une');
                      if( $thumb2 ):
                          // Thumbnail size attributes.
                          $size = '768-500';
                          $thumb2 = $thumb2['sizes'][ $size ];
                      endif; if( have_rows('informations') ): ?>
                        <?php while( have_rows('informations') ): the_row(); ?>

                       

          <div class="col-md-6 col-lg-4 block-photo <?php foreach(get_the_terms($query->post->ID, 'type') as $term)
         echo $term->slug . ' '; ?>">
            <a href="<?php the_permalink(); ?>">
              <div>
                <div class="thumbnail-container">
                  <div class="thumbnail lazyload" data-bg="<?php echo $thumb2; ?>">
                    <div class="container-image">
                      <div class="fakeimg"></div>
                    </div>
                  </div>
                </div>
                <div class="content">
                  <div class="content-info">
                    <div class="date">
                      <i class="fa fa-calendar" aria-hidden="true"></i><?php the_sub_field('date'); ?>
                    </div>
                    <div class="lieu">
                      <i class="fa fa-map-marker" aria-hidden="true"></i><?php the_sub_field('localisation'); ?>
                    </div>
                  </div>
                  <h2><?php echo str_replace(' | ', '<br />', get_the_title()); ?></h2>
                </div>
              </div>
            </a>
          </div><!-- projets-->
          <?php endwhile; ?>
            <?php endif; ?>

          <?php endwhile; ?>
            <?php endif; ?>
            <?php if (function_exists("pagination")) {?>
            <div class="col-sm-12">
              <?php pagination($query->max_num_pages); ?>
            </div>
            <?php } ?>
      </div>


        </div>
</section>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php get_footer(); ?>
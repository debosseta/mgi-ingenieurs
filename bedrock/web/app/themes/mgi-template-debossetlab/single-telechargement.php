<?php get_header('telechargement'); ?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<style>
    main{
        margin-top:0;
    }
#download{
    width:100%;
    min-height:100%;
    min-height: 100vh;
    background:#138d9f;
    
}

#download .container{
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
}

#download .container .content{
    background: #fff;
    padding:40px;
    margin-top:50px;
    padding-bottom:20px;
}

.logo img{
    max-width:80%;
    width:100%;
}

.button{
    background: white;
    color:#fff;
    display:inline-block;
    padding:10px;
    border:1px solid  #138d9f;
    color:  #138d9f;
    cursor:pointer;
    transition:all 0.4s;
    margin-top: 25px;
    margin-bottom: 25px;
    font-size:14px;

}

.button:hover{
    background:  #138d9f;
    color:#fff;
    border:1px solid  #138d9f;
    color:  white;
}

.goback{
    color:grey;
    font-size:14px;
}

.partage{
    margin-bottom:15px;
}
</style>
<section id="download">
<div class="container">
    <div class="row align-items-center">
        <div class="col-sm-12 center">
            <a class="logo" href="<?php echo get_home_url();?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-white.svg" alt="<?php wp_title(''); ?>" /></a>
            <div class="content">
            <?php
            $upload_dir = wp_upload_dir();
            $tmpName = get_the_title();
            $destinationZip = $upload_dir['basedir'].'/telechargements/zip/'.$tmpName.'.zip';
            ?>
            <h1 class="partage">Votre fichier : </h1>
            <h3><?php the_field('zip_fichier'); ?></h3>
            <h4 style="font-weight:100;color:#000"><?php echo get_zip_originalsize($destinationZip); ?></h4>
            <form action="<?php echo admin_url( 'admin-post.php' ); ?>" method="post">
                <input type="hidden" name="action" value="downloadZip">
                <input type="hidden" name="zip" value="<?php the_field('zip_fichier'); ?>">
                <input type="hidden" name="permalink" value="<?php the_permalink(); ?>">
                <input type="submit" class="button" value="Télécharger">
            </form>

            <a class="goback" target="_blank" href="<?php echo get_home_url();?>">Visiter le site MGI ingénieurs</a>

</div>
        </div>
    </div>
</div>
</section>
<?php endwhile; ?>
<?php endif; ?>
<?php get_footer('telechargement'); ?>
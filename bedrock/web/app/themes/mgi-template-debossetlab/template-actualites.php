<?php /* Template Name: Actualités */ ?>

<?php get_header(); ?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<?php $home_title = get_the_title( get_option('page_on_front') ); ?>
<?php
    $image = get_field('bandeau_image_de_fond');
    $thumb = '';
    if( $image ):
        // Thumbnail size attributes.
        $size = 'home-1920-500';
        $thumb = $image['sizes'][ $size ];
    endif;
    ?>

<section id="projet-title" class="small lazyload" data-bg="<?php echo $thumb; ?>">
  <div class="container-image">
    <div class="fakeimg"></div>
  </div>
  <div class="background"></div>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <ul class="breadcrum" itemscope itemtype="http://schema.org/BreadcrumbList">
              <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<a itemprop="item" href="<?php echo get_home_url(); ?>">
			<span itemprop="name"><?php echo $home_title; ?></span>	
		</a> 
		<meta itemprop="position" content="1" />
	      </li>
              <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<span itemprop="name"><?php the_title(); ?></span>
		<meta itemprop="position" content="2" />
	      </li>
            </ul>
        <div class="title" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
          <h1><?php the_title(); ?></h1>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="news">
  <div class="container">
    <div class="row">
      <div id="masonry">
        <div class="col-lg-6 grid-sizer"></div>

        <?php

$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

              $args = array(
                'post_type' => 'actualites',
                'post_status' => 'publish',
                'posts_per_page' => 10, 
                'paged' => $paged
              );
              $query = new WP_Query( $args );
              if ( $query->have_posts() ) :
                  while ( $query->have_posts() ) :
                      $query->the_post(); ?>



<?php
    $image = get_field('actu_photo_a_la_une');
    $thumb = '';
    if( $image ):
        // Thumbnail size attributes.
        $size = 'news';
        $thumb = $image['sizes'][ $size ];
    endif;
    ?>

        <div class="col-lg-8 offset-lg-2 news" data-aos="fade-up">
        <?php if( $image ): ?>
          <div class="thumbnail-container">
            <img class="thumbnail lazyload" src="<?php echo $thumb; ?>">
            <div class="container-image">
              <div class="fakeimg"></div>
            </div>
          </div>
        <?php endif; ?>
          <div class="content">
            <div class="content-info">
              <div class="date">
                <i class="fa fa-calendar" aria-hidden="true"></i><?php echo get_the_date( 'd.m.Y' ); ?>
              </div>
            </div>
            <h2><?php the_title(); ?></h2>
            <?php the_field('actu_contenu'); ?>
        
      

          <?php if( have_rows('galerie_image') ):?>
        <div class="gallery">
                  <div class="row row-photos">
                  <?php while( have_rows('galerie_image') ) : the_row(); ?>


<?php
    $image = get_sub_field('photo');
    $thumb = '';
    if( $image ):
        $size = '518-245';
        $thumb = $image['sizes'][ $size ];
        $size = 'large';
        $large = $image['sizes'][ $size ];
        $height = $image['sizes'][ 'large-height' ];
        $width = $image['sizes'][ 'large-width' ];
    endif;

    ?>
                 <div class="col-sm-6 col-lg-4 block-photo" data-aos="fade" data-aos-delay="0">
            <a href="<?php echo $large; ?>" data-download-url="false" data-width="<?php echo $width; ?>" data-height="<?php echo $height; ?>">
              <div class="thumbnail lazyload" data-bg="<?php echo $thumb; ?>">
                <div class="container-image">
                  <div class="fakeimg"></div>
                </div>
              </div>
            </a>
          </div>
                    
                 
                <?php endwhile; ?>
   </div>
   </div>
                <?php endif; ?>

                </div><!-- / news -->
                </div>




      







<?php endwhile; ?>
<?php endif; ?>


<?php if (function_exists("pagination")) {?>
            <div class="col-sm-12">
              <?php pagination($query->max_num_pages); ?>
            </div>
            <?php } ?>
      </div>
    </div>
</div>
</section>

<?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>
<?php /* Template Name: Bureau */ ?>
<?php get_header(); ?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<?php $home_title = get_the_title( get_option('page_on_front') ); ?>
<?php
$image = get_field('bandeau_image_de_fond');
$thumb = '';
if( $image ):
    $size = 'home-1920-500';
    $thumb = $image['sizes'][ $size ];
endif;
?>
<section id="projet-title">
      <div class="background" style="background-image: url('<?php echo $thumb; ?>')"></div>
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
 
            <?php $terms = get_the_terms($post->ID, 'type' ); ?>
          
		<ul class="breadcrum" itemscope itemtype="http://schema.org/BreadcrumbList">
              <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<a itemprop="item" href="<?php echo get_home_url(); ?>">
			<span itemprop="name"><?php echo $home_title; ?></span>	
		</a> 
		<meta itemprop="position" content="1" />
	      </li>
		<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<a itemprop="item" href="<?php echo esc_url( get_page_link( 43 ) ); ?>">
			<span itemprop="name"><?php echo get_the_title( 43 ); ?></span>	
		</a> 
		<meta itemprop="position" content="2" />
	      </li>
		<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<a itemprop="item" href="<?php echo esc_url( get_page_link( 43 ) ).$terms[0]->slug ; ?>">
			<span itemprop="name"><?php echo $terms[0]->name; ?></span>	
		</a> 
		<meta itemprop="position" content="3" />
	      </li>
              <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<span itemprop="name"><?php the_title(); ?></span>
		<meta itemprop="position" content="4" />
	      </li>
            </ul>
            <div class="title" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
              <h1><?php echo str_replace(' | ', '<br />', get_the_title()); ?></h1>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="projet-content" data-aos="fade-up" data-aos-delay="300">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 extend-right">
          <?php
            $image = get_field('photo_a_la_une');
            $thumb = '';
            if( $image ):
                $size = '768-500';
                $thumb = $image['sizes'][ $size ];
            endif;
            ?>
            <div class="thumbnail lazyload" data-bg="<?php echo $thumb; ?>">
              <div class="container-image">
                <div class="fakeimg"></div>
              </div>
            </div>
          </div>
          <div id="sidebar" class="col-lg-4 information sticky">
            <div class="sticky-wrap">
              <div class="sticky-content">
                <?php if( have_rows('informations') ): while( have_rows('informations') ) : the_row();?>
                <h2 data-aos="fade-up" data-aos-delay="300">Informations</h2>
                <?php if(get_sub_field('maitre_douvrage')): ?>
                <div class="row information-row" data-aos="fade-up" data-aos-delay="400">
                    <div class="col-md-6 col-sm-12">
                        <h4>Maître d'ouvrage</h4>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <p><?php the_sub_field('maitre_douvrage'); ?></p>
                    </div>
                </div>
                <?php endif; ?>
                <?php if(get_sub_field('mandataire')): ?>
                <div class="row information-row" data-aos="fade-up" data-aos-delay="400">
                    <div class="col-md-6 col-sm-12">
                        <h4>Mandataire</h4>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <p><?php the_sub_field('mandataire'); ?></p>
                    </div>
                </div>
                <?php endif; ?>
                <?php if(get_sub_field('date')): ?>
                <div class="row information-row" data-aos="fade-up" data-aos-delay="400">
                    <div class="col-md-6 col-sm-12">
                        <h4>Date</h4>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <p><?php the_sub_field('date'); ?></p>
                    </div>
                </div>
                <?php endif; ?>
                <?php if(get_sub_field('localisation')): ?>
                <div class="row information-row" data-aos="fade-up" data-aos-delay="400">
                    <div class="col-md-6 col-sm-12">
                        <h4>Localisation</h4>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <p><?php the_sub_field('localisation'); ?></p>
                    </div>
                </div>
                <?php endif; ?>
                <?php if(get_sub_field('phases_sia')): ?>
                <div class="row information-row" data-aos="fade-up" data-aos-delay="400">
                    <div class="col-md-6 col-sm-12">
                        <h4>Phases SIA</h4>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <p><?php the_sub_field('phases_sia'); ?></p>
                    </div>
                </div>
                <?php endif; ?>
                <?php if(get_sub_field('montant_travaux')): ?>
                <div class="row information-row" data-aos="fade-up" data-aos-delay="400">
                    <div class="col-md-6 col-sm-12">
                        <h4>Montant travaux</h4>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <p><?php the_sub_field('montant_travaux'); ?></p>
                    </div>
                </div>
                <?php endif; ?>
                <?php if(get_sub_field('personne_de_reference')): ?>
                <div class="row information-row" data-aos="fade-up" data-aos-delay="400">
                    <div class="col-md-6 col-sm-12">
                        <h4>Personne de référence</h4>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <p><?php the_sub_field('personne_de_reference'); ?></p>
                    </div>
                </div>
                <?php endif; ?>
                <?php endwhile; endif; ?>
              </div>
            </div>
          </div>
          <div class="col-lg-8 extend-right">
              <?php if(get_field('contenu')){ ?>
            <div class="content" data-aos="fade-up" data-aos-delay="400">

             <?php the_field('contenu'); ?>

            </div>
              <?php } ?>
          </div>

        </div>
      </div>
    </section>

    <?php if( have_rows('galerie_photos') ):?>
    <section id="projet-photos" class="gallery">
      <div class="container">
        <div class="row row-photos">

        <?php while( have_rows('galerie_photos') ) : the_row(); ?>


        <?php
            $image = get_sub_field('photo');
            $thumb = '';
            if( $image ):
                $size = '518-245';
                $thumb = $image['sizes'][ $size ];
                $size = 'large';
                $large = $image['sizes'][ $size ];
                $height = $image['sizes'][ 'large-height' ];
                $width = $image['sizes'][ 'large-width' ];
            endif;

            ?>
           <div class="col-sm-6 col-lg-4 block-photo" data-aos="fade" data-aos-delay="0">
            <a href="<?php echo $large; ?>" data-download-url="false" data-width="<?php echo $width; ?>" data-height="<?php echo $height; ?>">
              <div class="thumbnail lazyload" data-bg="<?php echo $thumb; ?>">
                <div class="container-image">
                  <div class="fakeimg"></div>
                </div>
              </div>
            </a>
          </div>
          
    <?php endwhile; ?>
        </div>
      </div>
    </section>


<?php
$terms = get_the_terms( $post->ID, 'type' );


?>

<?php




$args = array(
  'post_type' => 'projets',
  'post_status' => 'publish',
  'posts_per_page' => 3, 
  'orderby'        => 'rand',
  'post__not_in'           => array(get_the_ID()),
  'tax_query' => array(
    array(
      'taxonomy' => 'type',
      'field' => 'slug',
      'terms' => $terms[0]->slug,
      'posts_per_page' => -1
    )
  )
);
$query = new WP_Query( $args );
if ( $query->have_posts() ) { ?>


    <section id="projet-liste" class="head-projets">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 titlehome" data-aos="fade-up">
            <h2 data-aos="fade-up" data-aos-delay="200">Autres projets <?php echo $terms[0]->name; ?></h2>
          </div>
        </div>
        <div class="row filters-result" data-aos="fade-up">

       <?php
                  while ( $query->have_posts() ) {
                      $query->the_post();
                     
                      $thumb2 = get_field('photo_a_la_une');
                      if( $thumb2 ):
                          // Thumbnail size attributes.
                          $size = '768-500';
                          $thumb2 = $thumb2['sizes'][ $size ];
                      endif;
                      
                       if( have_rows('informations') ): ?>
                        <?php while( have_rows('informations') ): the_row(); ?>
                     

                        


                          <div class="col-md-6 col-lg-4 block-photo geniecivil">
                            <a href="<?php the_permalink(); ?>">
                            <div>
                                <div class="thumbnail-container">
                                <div class="thumbnail lazyload" data-bg="<?php echo $thumb2; ?>">
                                    <div class="container-image">
                                    <div class="fakeimg"></div>
                                    </div>
                                </div>
                                </div>
                                <div class="content">
                                <div class="content-info">
                                    <div class="date">
                                    <i class="fa fa-calendar" aria-hidden="true"></i><?php the_sub_field('date'); ?>
                                    </div>
                                    <div class="lieu">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i><?php the_sub_field('localisation'); ?>
                                    </div>
                                </div>
                                <h2><?php echo str_replace(' | ', '<br />', get_the_title()); ?></h2>
                                </div>
                            </div>
                            </a>
                        </div><!-- block-photo -->
                        <?php 
                   
                 
                         endwhile;
                      endif;
                  }
             ?>


         
          <!-- end -->
          
        </div>
      </div>
    </section>

    <?php  }
              // Restore original post data.
              wp_reset_postdata();
              ?> 

   
   
    <?php endif; ?>
<?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>
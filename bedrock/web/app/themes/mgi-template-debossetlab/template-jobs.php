<?php /* Template Name: Jobs */ ?>

<?php get_header(); ?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<?php $home_title = get_the_title( get_option('page_on_front') ); ?>
<?php
    $image = get_field('bandeau_image_de_fond');
    $thumb = '';
    if( $image ):
        // Thumbnail size attributes.
        $size = 'home-1920-500';
        $thumb = $image['sizes'][ $size ];
    endif;
    $pageIDJob = get_the_ID();
    ?>

<section id="projet-title" class="small lazyload" data-bg="<?php echo $thumb; ?>">
      <div class="container-image">
        <div class="fakeimg"></div>
      </div>
      <div class="background"></div>
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <ul class="breadcrum" itemscope itemtype="http://schema.org/BreadcrumbList">
              <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<a itemprop="item" href="<?php echo get_home_url(); ?>">
			<span itemprop="name"><?php echo $home_title; ?></span>	
		</a> 
		<meta itemprop="position" content="1" />
	      </li>
              <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<span itemprop="name"><?php the_title(); ?></span>
		<meta itemprop="position" content="2" />
	      </li>
            </ul>
            <div class="title" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
              <h1><?php the_title(); ?></h1>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    
    <section id="jobs-content" data-aos="fade-up" data-aos-delay="300">
      <div class="container link">
        <div class="row">
          <div class="col-lg-8 extend-right">

            <!-- -->
            
            <?php
              $args = array(
                'post_type' => 'jobs',
                'post_status' => 'publish',
                'posts_per_page' => -1, 
                'tax_query'      => array(
                    array(
                        'taxonomy' => 'category',
                        'operator' => 'NOT EXISTS'
                    )
                )
              );
              $query = new WP_Query( $args );
              if ( $query->have_posts() ) :
                  while ( $query->have_posts() ) :
                      $query->the_post(); ?>
                     
                     <div class="content" data-aos="fade-up" data-aos-delay="400">
                        <?php the_field('contenu'); ?>
                        
                        <div class="center job-top">
                        <?php if( get_field('email', $pageIDJob)){ ?><a class="cta" href="mailto:<?php echo antispambot( get_field('email',  $pageIDJob) ) ; ?>"><?php the_field('email_texte', $pageIDJob); ?></a><?php } ?>
                        </div>

                    </div>

                  <?php endwhile; ?>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>

            <!-- -->
            
        
          </div>


          <div id="sidebar" class="col-lg-4 information">
            <div class="sticky-wrap">
              <div class="sticky-content">
                
                <?php
              $args = array(
                'post_type' => 'jobs',
                'post_status' => 'publish',
                'posts_per_page' => -1, 
                'category_name' => 'bloc-droite'
              );
              $query = new WP_Query( $args );
              if ( $query->have_posts() ) :
                  while ( $query->have_posts() ) :
                      $query->the_post(); ?>
                <h2 data-aos="fade-up" data-aos-delay="300"><?php the_title(); ?></h2>
                <div class="row information-row" data-aos="fade-up" data-aos-delay="400">
                  <div class="col-12">
                  <?php the_field('contenu'); ?>
                  </div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>
                <div class="row information-row" data-aos="fade-up" data-aos-delay="400">
                <div class="col-12">
                  
                        <div class="center job-top">
                        <?php if( get_field('email', $pageIDJob)){ ?><a class="cta" href="mailto:<?php echo antispambot( get_field('email',  $pageIDJob) ) ; ?>"><?php the_field('email_texte', $pageIDJob); ?></a><?php } ?>
                        </div>
                        </div>
                </div>
              </div>
            </div>
          </div>



        </div>
      </div>
    </section>
    <?php endwhile; ?>
            <?php endif; ?>
            <?php get_footer(); ?>
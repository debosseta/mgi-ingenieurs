<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <title><?php wp_title(''); echo ' | ';  bloginfo( 'name' ); ?></title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

  <?php $descriptionSEO = '';
	$idPost = get_the_ID();

  	
	if ( is_page_template( 'template-projets.php' ) ) {

		$descriptionSEO = 'Découvrez tous nos projets de Génie civil, Génie ferroviaire, Bâtiment, Ouvrages, Environnement.'; 

	} else if(is_tax( 'type' )){
	
		$descriptionSEO = 'Découvrez tous nos projets de Génie civil, Génie ferroviaire, Bâtiment, Ouvrages, Environnement.';

	}else if ( is_page_template( 'template-jobs.php' ) ) {

		$descriptionSEO = 'Découvrez toutes nos offres d\'emploi et postuler directement par email via notre site internet.'; 
	
	} else if ( is_page_template( 'template-contact.php' ) ) {

		$descriptionSEO = 'Retrouvez les adresses de nos bureaux à Châtel-St-Denis, Bulle, Villars-sur-Glâne, Romont et contactez directement nos collaborateurs.'; 
	
	} else if ( is_page_template( 'template-collaborateurs.php' ) ) {

		$descriptionSEO = 'Actuellement, le bureau MGI dispose de l’expérience et des compétences d’environ 32 collaborateurs, dont : - 13 ingénieurs EPF-HES (civil, environnement, géomètre) - 5 techniciens (chef de chantier, domaine du génie-civil) - 5 dessinateurs (génie civil et béton armé) - 2 secrétaires - 1 spécialiste informatique - 6 apprentis'; 
	
	}  else if ( is_page_template( 'template-bureau.php' ) ) {

		$descriptionSEO = 'MGI accorde une grande importance à l’ouverture d’esprit, la communication et l’écoute de l’autre. Ces valeurs sont à la base de la réussite de projets complexes et interdisciplinaires. La pluridisciplinarité et la mise en commun des compétences permettent au bureau de se positionner comme un interlocuteur unique pour le Maître d’Ouvrage, ceci de la conception à la réalisation du projet.';

	} else if ( is_page_template( 'template-actualites.php' ) ) {

		$descriptionSEO = 'Suivez l\'actualité de MGI ingénieurs, avec nos projets de Génie civil, Génie ferroviaire, Bâtiment, Ouvrages, Environnement. Nos recrutements et avancement de projets.';

	} else if ( is_page_template( 'taxonomy-type.php' ) ) {

		$descriptionSEO = 'Découvrez tous nos projets de Génie civil, Génie ferroviaire, Bâtiment, Ouvrages, Environnement.'; 

	} else if ( is_singular( 'projets' ) ) {
		$toconvert= get_field('contenu', $idPost ); 	
		$start = strpos($toconvert, '<p>');
		$end = strpos($toconvert, '</p>', $start);
		$descriptionSEO = html_entity_decode(strip_tags(substr($toconvert, $start, $end-$start+4)));
	} else if ( is_front_page()  ) {
		
		$descriptionSEO = 'MGI ingénieurs SA est un bureau d’ingénieurs conseils en génie-civil et environnement. Depuis 1969, le bureau fonde son succès sur le savoir et l’expérience de ses collaborateurs, la gestion rigoureuse des projets, la diversité des métiers et la proximité au client.';
	}
?>

  <meta name="description" content="<?php echo $descriptionSEO; ?>">

  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
  <header>
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="menu-flex">
            <a class="logo" href="<?php echo get_home_url();?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.svg" alt="<?php wp_title(''); ?>" /></a>
            <button class="menu" onclick="this.classList.toggle('opened');this.setAttribute('aria-expanded', this.classList.contains('opened'))" aria-label="Main Menu">
              <svg width="40" height="40" viewBox="0 0 100 100">
                <path class="line line1" d="M 20,29.000046 H 80.000231 C 80.000231,29.000046 94.498839,28.817352 94.532987,66.711331 94.543142,77.980673 90.966081,81.670246 85.259173,81.668997 79.552261,81.667751 75.000211,74.999942 75.000211,74.999942 L 25.000021,25.000058" />
                <path class="line line2" d="M 20,50 H 80" />
                <path class="line line3" d="M 20,70.999954 H 80.000231 C 80.000231,70.999954 94.498839,71.182648 94.532987,33.288669 94.543142,22.019327 90.966081,18.329754 85.259173,18.331003 79.552261,18.332249 75.000211,25.000058 75.000211,25.000058 L 25.000021,74.999942" />
              </svg>
            </button>
            <div class="menu-container">
              <div class="container">
                <ul class="menu link">
                    <?php wp_mgi_menu(); ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <main>
<?php
add_filter('acf/load_field/name=date_expiration', 'my_acf_update_value', 10, 3);
add_filter('acf/load_field/name=zip_fichier', 'my_acf_update_value', 10, 3);
add_filter('acf/load_field/name=zip_taille', 'my_acf_update_value', 10, 3);

add_action('admin_enqueue_scripts', 'telechargementsJS');
add_action('wp_footer', 'telechargementsJS');
add_action('admin_enqueue_scripts', 'telechargementsCSS');
add_action('wp_dashboard_setup', 'telechargement_widget');

add_action('wp_ajax_dropFile', 'dropFile'); 
add_action('wp_ajax_nopriv_dropFile', 'dropFile');

add_action('wp_ajax_generateZip', 'generateZip'); 
add_action('wp_ajax_nopriv_generateZip', 'generateZip');


add_action('admin_post_downloadZip', 'downloadZip'); 

function my_acf_update_value($field ){

    $field['readonly'] = true;
    $field['disabled'] = true;
    return $field;
}

function telechargementsCSS( $hook ) {
    wp_enqueue_style(
    'telechargements.css',
    get_stylesheet_directory_uri() . '/assets/css/telechargements.css', array(), '1.6' );

    wp_enqueue_style(
        'dropzone',
        'https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/dropzone.css', array(), '1.3' );

        wp_enqueue_style(
            'dropzone',
            'https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/basic.min.css', array(), '1.3' );
    

        




 



}

function telechargementsJS( $hook ) {
    wp_enqueue_script( 'telechargements', get_stylesheet_directory_uri() . 
    '/assets/js/telechargements.js', array(), '3.0.4', true );
    wp_localize_script('telechargements', 'ajax_var', array(
        'url' => admin_url('admin-ajax.php'),
        'nonce' => wp_create_nonce('ajax-nonce')
    ));

    wp_enqueue_script( 'dropzone',
    'https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/min/dropzone.min.js', array(), '1.1.0', true );

    wp_enqueue_script('copy',
    'https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js', array(), '1.0.0', true );

    wp_enqueue_script('md5',
    'https://cdnjs.cloudflare.com/ajax/libs/blueimp-md5/2.10.0/js/md5.js', array(), '1.0.0', true );


}




function telechargement_widget() {
    global $wp_meta_boxes;
    wp_add_dashboard_widget('telechargementblock', 'Téléchargement', 'custom_dashboard_telechargement');
}
 
function custom_dashboard_telechargement() {
?>
<div class="uploadMGI">
    <?php $time = date('dmYHis');
    //$title = sanitize_title($title);
    $titleSlug = sanitize_title($time). rand(); ?>
    <?php $folderrandom = 'mgi-'.$titleSlug; ?>
    <form id="uploadingtelechargements"  class="dropzone" method="post" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" enctype="multipart/form-data">
        <input type="hidden" name="action" value="dropFile" />
        <input type="hidden" id="folderrandom" name="folderrandom" value="<?php echo $folderrandom; ?>">
        <input type="hidden" id="path" name="path" value="" />
    </form>
    <input type="button" id="submit_telechargements" value='Générer votre lien de partage de téléchargement' class="button button-primary button-large">
    <div id="telechargement"></div>
</div>
<?php
}

// Call everytime a file is drop
function dropFile() {
    // upload dir wp
    $upload_dir = wp_upload_dir();
    // set telechgargement tmp dir
    $upload_location = $upload_dir['basedir'].'/telechargements/tmp/';
    // get folderrandom name for current upload
    $folderrandom = $_POST['folderrandom'];
    // get path to the file to keep folder structure
    $pathFile = $folderrandom.'/'.$_POST['path']; 
    // If $_FILES
    if (!empty($_FILES)) {
        // Get tmp_name of current file
        $tempFile = $_FILES['file']['tmp_name']; 
        // telechargement/tmp/namerandom/folder/etc...
        $tempFolder =  $upload_location.$pathFile;
        //If doesn't exist mkdir
        if (!is_dir($tempFolder) && !mkdir($tempFolder,  0777, true) ){
            die("Erreur création du de la racine $tempFolder");
        }
        // Move tempfile to telechargement/tmp/folder/etc.../file.extension
        move_uploaded_file($tempFile,$tempFolder.$_FILES['file']['name']);
    }
    exit;
}

// Generate Zip file + remove tmp files
function generateZip(){
    $upload_dir = wp_upload_dir();
    // zip file name
    $tmpName = $_POST['tmpName'];
    // tmp folder where folder + files have been saved
    $tmpFolder = $upload_dir['basedir'].'/telechargements/tmp/'.$tmpName.'/';
    // Desination zip name
    $destinationZip = $upload_dir['basedir'].'/telechargements/zip/'.$tmpName.'.zip';
    // Generate the zip file
    //zipFile($tmpFolder,$destinationZip, true);
    $zipFile = new \PhpZip\ZipFile();
    try{
        $zipFile->addDirRecursive($tmpFolder);
        $zipFile->saveAsFile($destinationZip);
    }
    catch(\PhpZip\Exception\ZipException $e){
        // handle exception
    }
    finally{
        $zipFile->close();
    }
    // Delete tmp files
    rrmdir($tmpFolder);
    //Zip size
    //$zipSize = get_zip_originalsize($destinationZip);
    // Create telechargement
    createTelechargement($tmpName, $zipSize);
    // Die when don't need anymore
    wp_die();
}

// Get zip sizes
function get_zip_originalsize($filename) {
    $size = 0;
    $resource = zip_open($filename);
    while ($dir_resource = zip_read($resource)) {
        $size += zip_entry_filesize($dir_resource);
    }
    zip_close($resource);
    $sizeMb = ($size / 1024 / 1024);
    $sizeMb = number_format($sizeMb, 3);

    return $sizeMb.' MB';
}

// Create telechargement page to wordpress
function createTelechargement($tmpName, $zipSize){
    $post_id =  wp_insert_post(array(
        'post_title'=>$tmpName, 
        'post_type'=>'telechargement', 
        'post_status'   => 'publish',
        'post_name' =>  md5( time() )
    ));
    // Save name zip to make it secure no direct download
    update_field('zip_fichier', $tmpName.'.zip', $post_id);
    // Save expiration date
    update_field('date_expiration', expireDateTime(), $post_id);
    // Size zip
    update_field('zip_taille', $zipSize, $post_id);
    // Return link + copy button
    echo '<a data-copy="'.get_permalink($post_id).'" href="'.get_permalink($post_id).'" target="_blank">'.get_permalink($post_id).'</a>';
    echo '<button class="copy button" data-clipboard-text="'.get_permalink($post_id).'">Copier le lien</button>';
    // Die when don't need anymore
    wp_die();
}




/**
 * function zipFile.  Creates a zip file from source to destination
 *
 * @param  string $source Source path for zip
 * @param  string $destination Destination path for zip
 * @param  string|boolean $flag OPTIONAL If true includes the folder also
 * @return boolean
 */
function zipFile($source, $destination, $flag = '')
{
    if (!extension_loaded('zip') || !file_exists($source)) {
        return false;
    }

    $zip = new ZipArchive();
    if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
        return false;
    }

    $source = str_replace('\\', '/', realpath($source));
    if($flag)
    {
        $flag = basename($source) . '/';
        //$zip->addEmptyDir(basename($source) . '/');
    }

    if (is_dir($source) === true)
    {
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);
        foreach ($files as $file)
        {
            $file = str_replace('\\', '/', realpath($file));

            if (is_dir($file) === true)
            {
                //$zip->addEmptyDir(str_replace($source . '/', '', $flag.$file . '/'));
            }
            else if (is_file($file) === true)
            {
                $zip->addFromString(str_replace($source . '/', '', $flag.$file), file_get_contents($file));
            }
        }
    }
    else if (is_file($source) === true)
    {
        $zip->addFromString($flag.basename($source), file_get_contents($source));
    }

    return $zip->close();
}

function rrmdir($dir) { 
    if (is_dir($dir)) { 
      $objects = scandir($dir);
      foreach ($objects as $object) { 
        if ($object != "." && $object != "..") { 
          if (is_dir($dir. DIRECTORY_SEPARATOR .$object) && !is_link($dir."/".$object))
            rrmdir($dir. DIRECTORY_SEPARATOR .$object);
          else
            unlink($dir. DIRECTORY_SEPARATOR .$object); 
        } 
      }
      rmdir($dir); 
    } 
  }


function expireDateTime(){
    date_default_timezone_set('Europe/Paris');
    $time = date('Ymd');
    $dateUnix = strtotime($time. ' + 15 days');
    $date2 = new DateTime();
    $date2->setTimestamp($dateUnix);
    return $date2->format('Ymd');
}




wp_schedule_event( time(), 'daily', 'delete_expired_telechargement' );


function myprefix_custom_cron_schedule( $schedules ) {
    $schedules['every_six_hours'] = array(
        'interval' => 21600, // Every 6 hours
        'display'  => __( 'Every 6 hours' ),
    );
    return $schedules;
}
add_filter( 'cron_schedules', 'myprefix_custom_cron_schedule' );

//Schedule an action if it's not already scheduled
if ( ! wp_next_scheduled( 'myprefix_cron_hook' ) ) {
    wp_schedule_event( time(), 'every_six_hours', 'myprefix_cron_hook' );
}

///Hook into that action that'll fire every six hours
 add_action( 'myprefix_cron_hook', 'delete_expired_telechargement' );


function delete_expired_telechargement() 
{


    $today = date('Ymd', current_time('timestamp', 0));

    $telechargement = new WP_Query( array( 
        "post_type" => "telechargement",
        "post_status" => "publish",
        'posts_per_page' => -1,
        'meta_query'=> array(
            array(
              'key' => 'date_expiration',
              'compare' => '<',
              'value' => $today,
              'type' => 'DATE'
            )
        ),
    ) );

    if( $telechargement->post_count ) {
        foreach($telechargement->posts as $post) {
            // change post status to expired.
           

            $zipfichier = get_field('zip_fichier', $post->ID );
            if ($zipfichier){
                $upload_dir = wp_upload_dir();
                // Desination zip name
                $destinationZip = $upload_dir['basedir'].'/telechargements/zip/'.$zipfichier;
                // Delete Zip
                unlink($destinationZip);
            }

            wp_delete_post($post->ID);
        } // endforeach
    } // endif

   
 


}









function downloadZip(){

    // Téléchargement ici
   // $upload_dir = wp_upload_dir();

    //$filepath = '../../app/uploads/telechargements/zip/'.$_POST['zip'];
    $upload_dir = wp_upload_dir();
    $zipFile = '../../app/uploads/telechargements/zip/'.$_POST['zip'];
   var_dump($zipFile);
  //  var_dump($_POST['permalink']);


    download_file($zipFile);

    wp_redirect($_POST['permalink']);

   die();


}

function download_file( $fullPath )
{
  if( headers_sent() )
    die('Headers Sent');

   
  if(ini_get('zlib.output_compression'))
    ini_set('zlib.output_compression', 'Off');


  if( file_exists($fullPath) )
  {

    $fsize = filesize($fullPath);
    $path_parts = pathinfo($fullPath);
    $ext = strtolower($path_parts["extension"]);

    switch ($ext) 
    {
      case "pdf": $ctype="application/pdf"; break;
      case "exe": $ctype="application/octet-stream"; break;
      case "zip": $ctype="application/zip"; break;
      case "doc": $ctype="application/msword"; break;
      case "xls": $ctype="application/vnd.ms-excel"; break;
      case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
      case "gif": $ctype="image/gif"; break;
      case "png": $ctype="image/png"; break;
      case "jpeg":
      case "jpg": $ctype="image/jpg"; break;
      default: $ctype="application/force-download";
    }

    header("Pragma: public"); 
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private",false); 
    header("Content-Type: $ctype");
    header("Content-Disposition: attachment; filename=\"".basename($fullPath)."\";" );
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: ".$fsize);
    ob_clean();
    flush();
    readfile( $fullPath );

  } 
  else
    die('Erreur zip non existant contacter debosset@gmail.com');

}


function directory_skip_trash($post_id) {
    if (get_post_type($post_id) == 'telechargement') {
        // Force delete
        wp_delete_post( $post_id, true );
    }
} 

add_action('trashed_post', 'directory_skip_trash');


add_action( 'before_delete_post', 'deleteZipAttached' );
function deleteZipAttached( $postid ){

if (get_post_type($postid) != 'telechargement') {
return;
}





    $zipfichier = get_field('zip_fichier', $postid );
    if ($zipfichier){
        $upload_dir = wp_upload_dir();
        // Desination zip name
        $destinationZip = $upload_dir['basedir'].'/telechargements/zip/'.$zipfichier;
        // Delete Zip
        unlink($destinationZip);
    }
}


// Custom column in admin
function ST4_columns_telechargement_head($defaults) {
    unset($defaults['date']);
    $defaults['linktelechargement']  = 'Téléchargement URL';
    $defaults['copybtn'] = 'Partage';
    $defaults['expiration'] = 'Expiration';
    return $defaults;
}
 
function ST4_columns_telechargement_content($column_name, $post_ID) {

    // print_r($defaults);
    
    if ($column_name == 'linktelechargement') {?>
        <a href="<?php the_permalink($post_ID); ?>" target="_blank"><?php the_permalink($post_ID); ?></a>
    <?php }
    if ($column_name == 'copybtn') {
        // Second column
        echo '<button class="copy button" data-clipboard-text="'.get_permalink($post_ID).'">Copier le lien</button>';
    }
    if ($column_name == 'expiration') {
        // Second column
        $dateUnix = get_field('date_expiration');

        echo date("d.m.Y", strtotime($dateUnix));


    }
}

add_filter('manage_telechargement_posts_columns', 'ST4_columns_telechargement_head');
add_action('manage_telechargement_posts_custom_column', 'ST4_columns_telechargement_content', 10, 2);


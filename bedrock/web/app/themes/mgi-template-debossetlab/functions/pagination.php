<?php
function pagination($pages = '', $range = 4)
{
    $showitems = ($range * 2)+1;
 
    global $paged;
    if(empty($paged)) $paged = 1;
 
    if($pages == '')
    {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages)
        {
            $pages = 1;
        }
    }
 
    if(1 != $pages)
    {
        echo "<div class=\"pagination\"><span>Page ".$paged." sur ".$pages."</span>";
        if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; Première</a>";
        if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Précédente</a>";
 
        for ($i=1; $i <= $pages; $i++)
        {
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
            {
                echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
            }
        }
 
        if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>";
        if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Suivante &raquo;</a>";
        echo "</div>\n";
    }
}


function page_rewrite_pagination_slug(){
    // Add the slugs of the pages that are using a Global Template to simulate being an "archive" page
    $pseudo_archive_pages = array(
        "projets",
        "actualites",
    );

    $slug_clause = implode( "|", $pseudo_archive_pages );
    add_rewrite_rule( "($slug_clause)/page/([0-9]{1,})/?$", 'index.php?pagename=$matches[1]&paged=$matches[2]', "top" );
}
add_action( 'init', 'page_rewrite_pagination_slug' );

/*

function projets_cpt_generating_rule($wp_rewrite) {
    $rules = array();
    $terms = get_terms( array(
        'taxonomy' => 'type',
        'hide_empty' => false,
    ) );
   
    $post_type = 'projets';

    foreach ($terms as $term) {    
                
        $rules['projets/' . $term->slug . '/page/([0-9]{1,})/?$'] = 'index.php?post_type='.$post_type.'&type='.$term->slug.'&paged=$matches[1]';
                        
    }

    //var_dump($rules);
    //die();

    // merge with global rules
    $wp_rewrite->rules = $rules + $wp_rewrite->rules;
}*/

//add_rewrite_rule('^projets/([^/]*)/?','index.php?type=$matches[1]','top');


function wpd_property_rules(){
    add_rewrite_rule(
        'projets/([^/]+)/page/?([0-9]{1,})/?$',
        'index.php?post_type=projets&type=$matches[1]&paged=$matches[2]',
        'top'
    );
}
add_action( 'init', 'wpd_property_rules' );




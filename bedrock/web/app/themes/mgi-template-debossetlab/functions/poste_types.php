<?php
// Add Our Post Type Here

add_action( 'init', 'taxo', 0 );
// Actions
add_action( 'init', 'posteType', 0 );


function taxo() {
    register_taxonomy( 'position', 'collaborateurs', array(
        'label'        => __( 'Position', 'mgi' ),
        'rewrite'      => array( 'slug' => 'position' ),
        'hierarchical' => true
	) );
	register_taxonomy( 'type', 'projets', array(
        'label'        => __( 'Type', 'mgi' ),
        'rewrite'      => array( 'slug' => 'type' ),
		'hierarchical' => true,
		'slug' => 'projets', 
		'with_front' => false
    ) );
}

function wpa_show_permalinks( $post_link, $post ){
    if ( is_object( $post ) && $post->post_type == 'projets' ){
        $terms = wp_get_object_terms( $post->ID, 'type' );
        if( $terms ){
            return str_replace( '%type%' , $terms[0]->slug , $post_link );
        }
    }
    return $post_link;
}
add_filter( 'post_type_link', 'wpa_show_permalinks', 1, 2 );

function posteType() {

	$labels_actu = array(
		'name'                  => _x( 'Actualités', 'Actualités', 'mgi' ),
		'singular_name'         => _x( 'Actualité', 'Actualité', 'mgi' ),
		'menu_name'             => __( 'Actualités', 'mgi' ),
		'name_admin_bar'        => __( 'Actualités', 'mgi' ),
		'archives'              => __( 'Actualités Archives', 'mgi' ),
		'attributes'            => __( 'Actualités Attribut', 'mgi' ),
		'all_items'             => __( 'Toutes les actualités', 'mgi' ),
		'add_new_item'          => __( 'Ajouter une nouvelle actualité', 'mgi' ),
		'add_new'               => __( 'Ajouter une nouvelle actualité', 'mgi' ),
		'new_item'              => __( 'New Item', 'mgi' ),
		'edit_item'             => __( 'Modifier l\'actualité', 'mgi' ),
		'update_item'           => __( 'Mettre à jour l\'actualité', 'mgi' ),
		'view_item'             => __( 'Voir l\'actualité', 'mgi' ),
		'view_items'            => __( 'Voir les actualités', 'mgi' ),
		'search_items'          => __( 'Rechercher une actualité', 'mgi' ),
		'not_found'             => __( 'Pas trouvé', 'mgi' ),
		'not_found_in_trash'    => __( 'Pas trouvé dans la corbeille', 'mgi' ),
	);
	$args_actu = array(
		'label'                 => __( 'Nom exemple', 'mgi' ),
		'description'           => __( 'Description', 'mgi' ),
		'labels'                => $labels_actu,
		'supports'              => array( 'title'), // 'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ,'headway-seo') 
		'hierarchical'          => false,	
		'public' => false,  // it's not public, it shouldn't have it's own permalink, and so on
		'publicly_queryable' => true,  // you should be able to query it
		'show_ui' => true,  // you should be able to edit it in wp-admin
		'exclude_from_search' => true,  // you should exclude it from search results
		'show_in_nav_menus' => false,  // you shouldn't be able to add it to menus
		'has_archive' => false,  // it shouldn't have archive page
		'rewrite' => false,  // it shouldn't have rewrite rules
		'show_in_menu'          => true,
		'menu_position'         => 50,
		'menu_icon'             => 'dashicons-media-document',
		'show_in_admin_bar'     => true,
		'can_export'            => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		//'taxonomies'  => array( 'category' ),
	);
    register_post_type( 'actualites', $args_actu );
    
    $labels_projets= array(
		'name'                  => _x( 'Projets', 'Projets', 'mgi' ),
		'singular_name'         => _x( 'Projet', 'Projets', 'mgi' ),
		'menu_name'             => __( 'Projets', 'mgi' ),
		'name_admin_bar'        => __( 'Projets', 'mgi' ),
		'archives'              => __( 'Projets Archives', 'mgi' ),
		'attributes'            => __( 'Projets Attribut', 'mgi' ),
		'all_items'             => __( 'Tous les Projets', 'mgi' ),
		'add_new_item'          => __( 'Ajouter un nouveau Projet', 'mgi' ),
		'add_new'               => __( 'Ajouter un nouveau Projet', 'mgi' ),
		'edit_item'             => __( 'Modifier le projet', 'mgi' ),
		'update_item'           => __( 'Mettre à jour l\'actualité', 'mgi' ),
		'view_item'             => __( 'Voir le projet', 'mgi' ),
		'view_items'            => __( 'Voir les projets', 'mgi' ),
		'search_items'          => __( 'Rechercher un projet', 'mgi' ),
		'not_found'             => __( 'Pas trouvé', 'mgi' ),
		'not_found_in_trash'    => __( 'Pas trouvé dans la corbeille', 'mgi' ),
	);
	$args_projets = array(
		'label'                 => __( 'Nom exemple', 'mgi' ),
		'description'           => __( 'Description', 'mgi' ),
		'labels'                => $labels_projets,
		'supports'              => array(), // 'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ,'headway-seo') 
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 45,
		'menu_icon'             => 'dashicons-admin-tools',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'taxonomies'  => array( 'type' ),
		'rewrite' => array( 'slug' => 'projets/%type%', 'with_front' => false ),
		
	);
	register_post_type( 'projets', $args_projets );



	$labels_jobs= array(
		'name'                  => _x( 'Jobs', 'Jobs', 'mgi' ),
		'singular_name'         => _x( 'Job', 'Jobs', 'mgi' ),
		'menu_name'             => __( 'Jobs', 'mgi' ),
		'name_admin_bar'        => __( 'Job', 'mgi' ),
		'archives'              => __( 'Job Archives', 'mgi' ),
		'attributes'            => __( 'Job Attribut', 'mgi' ),
		'all_items'             => __( 'Tous les Jobs', 'mgi' ),
		'add_new_item'          => __( 'Ajouter un nouveau Job', 'mgi' ),
		'add_new'               => __( 'Ajouter un nouveau Job', 'mgi' ),
		'edit_item'             => __( 'Modifier le Job', 'mgi' ),
		'update_item'           => __( 'Mettre à jour le Job', 'mgi' ),
		'view_item'             => __( 'Voir le Job', 'mgi' ),
		'view_items'            => __( 'Voir les Jobs', 'mgi' ),
		'search_items'          => __( 'Rechercher un Job', 'mgi' ),
		'not_found'             => __( 'Pas trouvé', 'mgi' ),
		'not_found_in_trash'    => __( 'Pas trouvé dans la corbeille', 'mgi' ),
	);
	$args_jobs = array(
		'label'                 => __( 'Nom exemple', 'mgi' ),
		'description'           => __( 'Description', 'mgi' ),
		'labels'                => $labels_jobs,
		'supports'              => array( 'title'), // 'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ,'headway-seo') 
		'hierarchical'          => false,	
		'public' => false,  // it's not public, it shouldn't have it's own permalink, and so on
		'publicly_queryable' => true,  // you should be able to query it
		'show_ui' => true,  // you should be able to edit it in wp-admin
		'exclude_from_search' => true,  // you should exclude it from search results
		'show_in_nav_menus' => false,  // you shouldn't be able to add it to menus
		'has_archive' => false,  // it shouldn't have archive page
		'rewrite' => false,  // it shouldn't have rewrite rules
		'show_in_menu'          => true,
		'menu_position'         => 45,
		'menu_icon'             => 'dashicons-megaphone',
		'show_in_admin_bar'     => true,
		'can_export'            => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'taxonomies'  => array( 'category' ),
	);
	register_post_type( 'jobs', $args_jobs );

	$labels_collaborateur= array(
		'name'                  => _x( 'Collaborateurs', 'Collaborateurs', 'mgi' ),
		'singular_name'         => _x( 'Collaborateur', 'Collaborateurs', 'mgi' ),
		'menu_name'             => __( 'Collaborateurs', 'mgi' ),
		'name_admin_bar'        => __( 'Collaborateurs', 'mgi' ),
		'archives'              => __( 'Collaborateurs Archives', 'mgi' ),
		'attributes'            => __( 'Collaborateurs Attribut', 'mgi' ),
		'all_items'             => __( 'Tous les Collaborateurs', 'mgi' ),
		'add_new_item'          => __( 'Ajouter un nouveau Collaborateur', 'mgi' ),
		'add_new'               => __( 'Ajouter un nouveau Collaborateur', 'mgi' ),
		'edit_item'             => __( 'Modifier le Collaborateur', 'mgi' ),
		'update_item'           => __( 'Mettre à jour le Collaborateur', 'mgi' ),
		'view_item'             => __( 'Voir le Collaborateur', 'mgi' ),
		'view_items'            => __( 'Voir les Collaborateurs', 'mgi' ),
		'search_items'          => __( 'Rechercher un Collaborateur', 'mgi' ),
		'not_found'             => __( 'Pas trouvé', 'mgi' ),
		'not_found_in_trash'    => __( 'Pas trouvé dans la corbeille', 'mgi' ),
	);
	$args_collaborateur = array(
		'label'                 => __( 'Nom exemple', 'mgi' ),
		'description'           => __( 'Description', 'mgi' ),
		'labels'                => $labels_collaborateur,
		'supports'              => array( 'title'), // 'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ,'headway-seo') 
		'hierarchical'          => false,
			
		'public' => false,  // it's not public, it shouldn't have it's own permalink, and so on
'publicly_queryable' => true,  // you should be able to query it
'show_ui' => true,  // you should be able to edit it in wp-admin
'exclude_from_search' => true,  // you should exclude it from search results
'show_in_nav_menus' => false,  // you shouldn't be able to add it to menus
'has_archive' => false,  // it shouldn't have archive page
'rewrite' => false,  // it shouldn't have rewrite rules

		
		
		'show_in_menu'          => true,
		'menu_position'         => 45,
		'menu_icon'             => 'dashicons-id',
		'show_in_admin_bar'     => true,
		'can_export'            => true,
		
	
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'taxonomies'  => array( 'position' ),
	);
	register_post_type( 'collaborateurs', $args_collaborateur );


	$labels_telechargement= array(
		'name'                  => _x( 'Téléchargement', 'Jobs', 'mgi' ),
		'singular_name'         => _x( 'Téléchargement', 'Jobs', 'mgi' ),
		'menu_name'             => __( 'Téléchargement', 'mgi' ),
		'name_admin_bar'        => __( 'Téléchargement', 'mgi' ),
		'archives'              => __( 'Téléchargement', 'mgi' ),
		'attributes'            => __( 'Téléchargement', 'mgi' ),
		'all_items'             => __( 'Tous les Téléchargements', 'mgi' ),
		'add_new_item'          => __( 'Ajouter un nouveau Téléchargement', 'mgi' ),
		'add_new'               => __( 'Ajouter un nouveau Téléchargement', 'mgi' ),
		'edit_item'             => __( 'Modifier le Téléchargement', 'mgi' ),
		'update_item'           => __( 'Mettre à jour le Téléchargement', 'mgi' ),
		'view_item'             => __( 'Voir le Téléchargement', 'mgi' ),
		'view_items'            => __( 'Voir les Téléchargements', 'mgi' ),
		'search_items'          => __( 'Rechercher un Téléchargement', 'mgi' ),
		'not_found'             => __( 'Pas trouvé', 'mgi' ),
		'not_found_in_trash'    => __( 'Pas trouvé dans la corbeille', 'mgi' ),
	);
	$args_telechargement = array(
		'label'                 => __( 'Nom exemple', 'mgi' ),
		'description'           => __( 'Description', 'mgi' ),
		'labels'                => $labels_telechargement,
		'supports'              => array( 'title'), // 'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ,'headway-seo') 
		'hierarchical'          => false,	
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 45,
		'menu_icon'             => 'dashicons-cloud',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		"capability_type" => "telechargement",
		'capabilities' => array(
			    'publish_posts' => 'publish_telechargements',
				'edit_posts' => 'edit_telechargements',
				'edit_others_posts' => 'edit_others_telechargements',
				'delete_posts' => 'delete_telechargements',
				'delete_others_posts' => 'delete_others_telechargements',
				'read_private_posts' => 'read_private_telechargements',
				'edit_post' => 'edit_telechargements',
				'delete_post' => 'delete_telechargements',
				'read_post' => 'read_telechargements',
				'create_posts' => 'do_not_allow',
		),
		"map_meta_cap" => false,
		
	);
	register_post_type( 'telechargement', $args_telechargement );


}
add_action( 'restrict_manage_posts', 'my_restrict_manage_posts' );

function my_restrict_manage_posts() {
    global $typenow, $post, $post_id;

    if( $typenow != "page" && $typenow != "post" ){
        //get post type
        $post_type=get_query_var('post_type'); 

        //get taxonomy associated with current post type
        $taxonomies = get_object_taxonomies($post_type);

        //in next loop add filter for tax
        if ($taxonomies) {
            foreach ($taxonomies as $tax_slug) {
                $tax_obj = get_taxonomy($tax_slug);
                $tax_name = $tax_obj->name;
                $terms = get_terms($tax_slug);
                echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
                echo "<option value=''>Tous les ".$tax_name."</option>";
                foreach ($terms as $term) { 
                    $label = (isset($_GET[$tax_slug])) ? $_GET[$tax_slug] : ''; // Fix
                    echo '<option value='. $term->slug, $label == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>';
                }
                echo "</select>";
            }
        }
    }
}




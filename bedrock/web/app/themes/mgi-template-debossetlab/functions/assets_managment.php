<?php
// Actions
add_action('wp_enqueue_scripts', 'theme_scripts');
// Functions
function theme_scripts() {
    // Add main style
    wp_enqueue_style( 'google_font', 'https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400&display=swap');
    wp_enqueue_style( 'leafletcss', 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.5.1/leaflet.css');
    wp_enqueue_script('fontawesome', 'https://use.fontawesome.com/69b3390da2.js', array( 'jquery' ), '1.0', true);

    wp_enqueue_script('lightgallery', 'https://cdn.jsdelivr.net/combine/npm/lightgallery,npm/lg-fullscreen,npm/lg-hash,npm/lg-pager,npm/lg-zoom', array( 'jquery' ), '1.0', true);
   
    wp_enqueue_style( 'lightgallerycss', 'https://cdnjs.cloudflare.com/ajax/libs/lightgallery-js/1.2.0/css/lightgallery.min.css', array(), '1.6' );

    wp_enqueue_style( 'mgicss', get_stylesheet_directory_uri() . '/assets/css/main.css', array(), '3.0.3' );


    // Footer
    wp_enqueue_script('leafetjs', 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.5.1/leaflet.js', array( 'jquery' ), '1.5.1', true);
    wp_enqueue_script('mgijs', get_stylesheet_directory_uri() . '/assets/js/main.js', array( 'jquery' ), '1.5', true);
}

if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
   wp_deregister_script('jquery');
   wp_register_script('jquery', "https://code.jquery.com/jquery-3.5.1.min.js", false, null);
   wp_enqueue_script('jquery');
}
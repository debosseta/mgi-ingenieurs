<?php
// Register Menu Wordpress
add_action( 'init', 'widepress_register_menus' );
// Functions
function widepress_register_menus() {
    register_nav_menus(
      array(
        'main-menu' => __( 'Menu principal' )
      )
    );
  }

  function wp_mgi_menu()
  {
      $options = array(
          'echo' => false,
          'container' => false,
          'theme_location' => 'main-menu',
          'fallback_cb'=> 'fall_back_menu'
      );
  
      $menu = wp_nav_menu($options);
      echo preg_replace(array(
          '#^<ul[^>]*>#',
          '#</ul>$#'
      ), '', $menu);
  
  }
  
  function fall_back_menu(){
      return;
  }
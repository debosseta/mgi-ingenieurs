<?php

add_action('login_head', 'login_mgi');
add_action('admin_head', 'login_mgi');
add_filter( 'login_headerurl', 'wpb_login_logo_url' );
add_filter( 'login_headertitle', 'wpb_login_logo_url_title' );

function login_mgi() {
echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/assets/css/login.css" />';
}

function wpb_login_logo_url() {
    return home_url();
}

function wpb_login_logo_url_title() {
    return 'MGI Ingénieurs';
}
<?php
add_image_size( 'home-1920-500', 1920, 500, array( 'center', 'bottom' ) );
add_image_size( '768-500', 768, 500, array( 'center', 'center' ) );
add_image_size( '1920-200', 1920, 200, array( 'center', 'center' ) );
add_image_size( '200-200', 200, 200, FALSE );
add_image_size( '600-600', 600, 600, array( 'center', 'center' ) );
add_image_size( '283-283', 283, 283, array( 'center', 'center' ) );
add_image_size( '518-245', 518, 245, array( 'center', 'center' ) );
add_image_size('news', 768);
<?php
/**
 * Removing dashboard widgets.
 */
// Actions
add_action( 'admin_init', 'menuAdmin');
/*add_filter('login_redirect', 'redirectToPage', 10, 3);

function redirectToPage($redirect_to, $request, $user){
  $url = admin_url('edit.php?post_type=page');
  return($url);
}*/

function menuAdmin() {
  // Remove the 'Welcome' panel
  remove_action('welcome_panel', 'wp_welcome_panel');
  // Remove the 'At a Glance' metabox
  remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
  // Remove the 'Activity' metabox
  remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' );
  // Remove the 'WordPress News' metabox
  remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
  // Remove the 'Quick Draft' metabox
  remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
    remove_menu_page( 'uploads.php' );
    //remove_menu_page( 'index.php' );   
    remove_menu_page( 'upload.php' );
    remove_menu_page( 'tools.php' );             
    remove_menu_page( 'options-general.php' ); 
    remove_menu_page( 'edit.php' );
    //remove_menu_page( 'themes.php' );
    remove_menu_page( 'plugins.php' );
};

function remove_toolbar_node($wp_admin_bar) {
	
	// replace 'updraft_admin_node' with your node id
  $wp_admin_bar->remove_node('comments');
  $wp_admin_bar->remove_node('new-content');
	
}
add_action('admin_bar_menu', 'remove_toolbar_node', 999);

function remove_footer_admin () 
{
    echo '<span id="footer-thankyou">Développé par <a href="https://adrien.debosset.ch" target="_blank">Adrien de Bosset</a></span>';
}
 
add_filter('admin_footer_text', 'remove_footer_admin');


add_filter('acf/settings/show_admin', 'my_acf_show_admin');

function my_acf_show_admin( $show ) {
    
    return current_user_can('manage_options');
    
}

show_admin_bar(false);


add_action('wp_dashboard_setup', 'themeprefix_remove_dashboard_widget' );
/**
 *  Remove Site Health Dashboard Widget
 *
 */
function themeprefix_remove_dashboard_widget() {
    remove_meta_box( 'dashboard_site_health', 'dashboard', 'normal' );
}

add_action( 'admin_menu', 'remove_site_health_menu' );	
/**
 * Remove Site Health Sub Menu Item
 */
function remove_site_health_menu(){
  remove_submenu_page( 'tools.php','site-health.php' ); 
}

add_filter( 'wp_fatal_error_handler_enabled', '__return_false' );



function acf_review_before_save_post($post_id) {
	if( 'projets' == get_post_type($post_id) ){
    $_POST['acf']['_post_title'] = $_POST['acf']['field_5f27fcc5d6693'];
  }
	return $post_id;
}
add_action('acf/pre_save_post', 'acf_review_before_save_post', -1);



/**
 * Disable the emoji's
 */
function disable_emojis() {
 remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
 remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
 remove_action( 'wp_print_styles', 'print_emoji_styles' );
 remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
 remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
 remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
 remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
 add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
 add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 * 
 * @param array $plugins 
 * @return array Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
 if ( is_array( $plugins ) ) {
 return array_diff( $plugins, array( 'wpemoji' ) );
 } else {
 return array();
 }
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 * @return array Difference betwen the two arrays.
 */
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
 if ( 'dns-prefetch' == $relation_type ) {
 /** This filter is documented in wp-includes/formatting.php */
 $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

$urls = array_diff( $urls, array( $emoji_svg_url ) );
 }

return $urls;
}
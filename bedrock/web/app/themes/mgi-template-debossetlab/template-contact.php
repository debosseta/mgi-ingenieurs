<?php /* Template Name: Contact */ ?>
<?php get_header(); ?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<?php $home_title = get_the_title( get_option('page_on_front') ); ?>
<?php
    $image = get_field('bandeau_image_de_fond');
    $thumb = '';
    if( $image ):
        // Thumbnail size attributes.
        $size = 'home-1920-500';
        $thumb = $image['sizes'][ $size ];
    endif;
    ?>

<section id="projet-title" class="small lazyload" data-bg="<?php echo $thumb; ?>">
  <div class="container-image">
    <div class="fakeimg"></div>
  </div>
  <div class="background"></div>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
       <ul class="breadcrum" itemscope itemtype="http://schema.org/BreadcrumbList">
              <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<a itemprop="item" href="<?php echo get_home_url(); ?>">
			<span itemprop="name"><?php echo $home_title; ?></span>	
		</a> 
		<meta itemprop="position" content="1" />
	      </li>
              <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<span itemprop="name"><?php the_title(); ?></span>
		<meta itemprop="position" content="2" />
	      </li>
            </ul>
        <div class="title" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
          <h1><?php the_title(); ?></h1>
        </div>
      </div>
    </div>
  </div>
</section>

    <section id="contact">
      <div class="container link">
        <div class="row">

        
        <?php if( have_rows('bureau_listes') ): while( have_rows('bureau_listes') ) : the_row(); ?>
        <?php
        $image = get_sub_field('photo_du_bureau');
        $thumb = '';
        if( $image ):
            // Thumbnail size attributes.
            $size = '768-500';
            $thumb = $image['sizes'][ $size ];
        endif;
        ?>
          <!-- contact -->
          <div class="col-md-6 col-lg-3 contact" data-aos="fade-up">
            <div class="row">
              <div class="col-lg-12">
                <div class="thumbnail lazyload" data-bg="<?php echo $thumb; ?>">
                  <div class="container-image">
                    <div class="fakeimg"></div>
                  </div>
                </div>
              </div>
              <div id="sidebar" class="col-lg-12 information">
                <div class="sticky-wrap">
                  <div class="sticky-content">
                    <ul data-aos="fade-up" data-aos-delay="300">
                      <li class="adresse"><?php the_sub_field('rue'); ?><br />
                      <?php the_sub_field('code_postal_ville'); ?></li>
                      <li class="telephone"><?php the_sub_field('telephone'); ?></li>
                      <?php if(get_sub_field('lien_google_map')){ ?>
                      <li><a href="<?php the_sub_field('lien_google_map'); ?>" target="_blank"><?php the_sub_field('text_google_map_'); ?></a>
                      <?php } ?>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!--/ contact-->
          <?php endwhile; endif; ?>
          
        </div>
      </div>

      <div class="contact-collaborateur">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 center">
              <a href="<?php the_field('bouton_lien'); ?>" class="cta whitedark"><?php the_field('bouton_texte'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
            </div>
          </div>
        </div>
      </div>
      
      <div id="map"></div>
    </section>


<?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>
const pkg = require('./package.json')

module.exports = {
  namespaces: {
    '':       'src/components',       // ::slider/slider.twig
    'layout': 'src/layouts',          // layout::default.twig
    'node':   'node_modules',         // node::bootstrap
    '@wide':  'node_modules/@wide'    // @wide::slider/src/slider.twig
  },
  data: {
    site: {
      title: pkg.name
    }
  },
  functions: {
    svg: name => `<svg><use href="assets/sprite.svg#${name}"></svg>`
  }
}